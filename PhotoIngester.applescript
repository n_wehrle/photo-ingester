on open filelist
	tell application "Terminal"
		activate
		repeat with i in filelist
			do script "/usr/local/bin/ingest " & quoted form of POSIX path of i in front window
		end repeat
	end tell
end open
