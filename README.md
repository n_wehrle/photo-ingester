# Photoingester
I use photoingester to prepare my photos for the final lightroom import.
All photos of a connected device are transferred to a defined folder structure. The following metadata is taken into account to create a folder structure:

- RAW or JPEG
- Device name
- Date

## Example
![tree](https://bitbucket.org/n_wehrle/photo-ingester/raw/master/screen.png)

## Prerequisites
- must: exiftool
- nice to have: growl

## Usage
- Option 1: pass the folder name to the shellscript
- Option 2: drag the folder to the included drop-target