#!/bin/bash
if [ ! -d "$1" ]; then
    echo usage $0 folder
    exit 1
fi

TARGET_UNMANAGED="$HOME/Pictures/Unmanaged"
TARGET_IMPORT="$HOME/Pictures/Import"

UNMANAGED_COUNT_BEFORE=$(find $TARGET_UNMANAGED -type f \( ! -iname ".*" \) 2>/dev/null | wc -l | tr -d ' ')
IMPORT_COUNT_BEFORE=$(find $TARGET_IMPORT -type f \( ! -iname ".*" \) 2>/dev/null | wc -l | tr -d ' ')

DATEPATTERN_UNMANAGED="$TARGET_UNMANAGED/%%e/%Y/%Y%m/%Y%m%d/%Y%m%d"
DATEPATTERN_IMPORT="$TARGET_IMPORT/%Y%m%d"

echo === Ingest unmanaged RAWs
exiftool -o . -r -progress -ext RAF '-FileName<${CreateDate}_${ShortModel}_%-4f.%e' -d $DATEPATTERN_UNMANAGED $1
exiftool -if '$ShortModel ne "50D"' -o . -r -progress -ext CR2 '-FileName<${CreateDate}_${ShortModel}_%-4f.%e' -d $DATEPATTERN_UNMANAGED $1

echo === Ingest managed RAWs
exiftool -if '$ShortModel eq "50D"' -o . -r -progress -ext CR2  '-FileName<${CreateDate}_${ShortModel}_%-4f.%e' -d $DATEPATTERN_IMPORT $1

echo === Ingest managed JPGs
exiftool -o . -r -progress -ext JPG '-FileName<${CreateDate}_${ShortModel}_%-4f.%e' -d $DATEPATTERN_IMPORT $1

echo === Move managed files to camera subfolder
# ATTENTION: exiftool does not overwrite files if they already exist
exiftool -progress '-directory<%d${ShortModel}' $TARGET_IMPORT

UNMANAGED_COUNT_AFTER=$(find $TARGET_UNMANAGED -type f \( ! -iname ".*" \) 2>/dev/null | wc -l | tr -d ' ')
IMPORT_COUNT_AFTER=$(find $TARGET_IMPORT -type f \( ! -iname ".*" \) 2>/dev/null | wc -l | tr -d ' ')

DELTA_UNMANAGED=`expr $UNMANAGED_COUNT_AFTER - $UNMANAGED_COUNT_BEFORE`
DELTA_IMPORT=`expr $IMPORT_COUNT_AFTER - $IMPORT_COUNT_BEFORE`

echo === Results
RESULT_UNMANAGED="Unmanaged: $UNMANAGED_COUNT_AFTER (+$DELTA_UNMANAGED)"
RESULT_IMPORT="Ready for import: $IMPORT_COUNT_AFTER (+$DELTA_IMPORT)"

echo $RESULT_UNMANAGED $'\n'$RESULT_IMPORT
if type "growlnotify" &>/dev/null; then
  echo $RESULT_UNMANAGED $'\n'$RESULT_IMPORT | growlnotify -t "Ingest complete" -s -m -
fi
